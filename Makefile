CC=gcc

CFLAGS=-c -w
 
all: execute
	rm -rf *o askisi

execute: askisi
        #         resources/ avail/ processes/ steps_sec/ min_interval /max_interval /period
	./askisi 4 5 6 2 7 8 60 5 10 5
 
askisi: main.o functions.o
	$(CC) main.o functions.o -o askisi 

main.o:
	$(CC) $(CFLAGS) main.c

functions.o:
	$(CC) $(CFLAGS) functions.c 
