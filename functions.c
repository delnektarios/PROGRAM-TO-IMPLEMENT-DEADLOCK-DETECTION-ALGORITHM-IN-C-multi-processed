
//  NEKTARIOS DELIGIANNAKIS 1115201200030 sdi1200030@di.uoa.gr

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <signal.h>


// remove shared memory segments and semaphores
void cleanup (int semid,int shm_id1,int shm_id2,int shm_id3, int *addr,int *priority,int *in)
{
        semctl(semid, 0, IPC_RMID,0);
        fprintf(stderr,"\nSEMAPHORES REMOVED = %s\n",strerror(errno));
        fprintf(stderr,"\nREMOVING SHARED MEMORY = %s\n\n\n",strerror(errno));
        shmdt (addr);
        shmdt (priority);
        shmdt (in);
        shmctl (shm_id1, IPC_RMID, 0);
        shmctl (shm_id2, IPC_RMID, 0);
        shmctl (shm_id3, IPC_RMID, 0);
        return;
} // end cleanup function

//BOTH FOLLOWING FUNCTIONS ARE EDITED FROM http://suraj1693.blogspot.gr/2013/11/program-to-implement-deadlock-detection.html

//function to display current state of source-applicants
void show(int (*alloc)[50],int (*max)[50],int (*avail),int processes,int resources){
        int i,j;
        fprintf(stderr,"Process\t Allocation\t Max\t Available\t");
        for(i=0;i<processes;i++){
                printf("\nP%d\t   ",i+1);
                for(j=0;j<resources;j++){//i++;
                        printf("%d ",alloc[i][j]);
                }
                printf("\t");
                for(j=0;j<resources;j++){
                        printf("%d ",max[i][j]); 
                }
                printf("\t");
                if(i==0){
                        for(j=0;j<resources;j++)
                        printf("%d ",avail[j]);
                }
        }
}

//function to detect deadlock
void cal(int (*alloc)[50],int (*max)[50],int (*avail),int processes,int resources,int (*finish)){
        int need[50][50],flag=1,k;
        int dead[50];
        int i,j;
        fprintf(stderr,"********** Deadlock Detection Algorithm ************\n");
        //find need matrix
        for(i=0;i<processes;i++){
                for(j=0;j<resources;j++){
                        need[i][j]=0;
                }
        }
        for(i=0;i<processes;i++){
                for(j=0;j<resources;j++){
                        need[i][j]=max[i][j]-alloc[i][j];
                }
        }
        while(flag){
                flag=0;
                for(i=0;i<processes;i++){
                        int c=0;
                        for(j=0;j<resources;j++){
                                if((finish[i]==0)&&(need[i][j]<=avail[j])){
                                        c++;
                                        if(c==resources){
                                                for(k=0;k<resources;k++){ 
                                                        finish[i]=1;
                                                        flag=1;
                                                }
                                                if(finish[i]==1){
                                                        i=processes;
                                                }
                                        }
                                }
                        }
                }
        }
        j=0;
        flag=0;
        for(i=0;i<processes;i++){
                if(finish[i]==0){
                        dead[j]=i;
                        j++;
                        flag=1;
                }
        }
        if(flag==1){
                printf("\n\nSystem is in Deadlock !\n");

        }
        else{ printf("\nNo Deadlock Occured\n");}  
        return;
}
