
//  NEKTARIOS DELIGIANNAKIS 1115201200030 sdi1200030@di.uoa.gr

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <signal.h>
#include <time.h>

//resources/ avail/ processes/ steps_sec/ min_interval/ max_interval /period

int main(int argc,char *argv[]){

pid_t pid[50];
int i,interval,j,k,a,child_processes=atoi(argv[argc-5]);
int buffer[10],sem=0,shm_id1,shm_id2,shm_id3,shm_id4;
int alive_processes=child_processes;

int *addr = (int *) malloc (50 * sizeof (int));  //allocating space to be attached to shared mem segment1
int *priority = (int *) malloc (50*sizeof(int));        //allocating space to be attached to shared mem segment2
int *in = (int *) malloc (1*sizeof(int));               //allocating space to be attached to shared mem segment3


//seting up semaphores
//---------------------------------------------------------------------
union senum {
int  val;    
struct semid_ds *buf;    
unsigned short  *array;  
};

struct sembuf {
short sem_num;
short sem_op;
short sem_flg;
};

struct sembuf semopr[3];

int semid = semget((key_t) 333, 3, IPC_CREAT | 0660); //requesting semaphores
if(semid==-1) {fprintf(stderr,"ALLOCATING THE SEMAPHORES = %s\n",strerror(errno)); exit(-1);}
fprintf(stderr,"ALLOCATING THE SEMAPHORES = %s\n",strerror(errno));

//for the critical section
union senum semaphore1;
semaphore1.val=1;
semctl(semid, 0, SETVAL, semaphore1);

//for blocking the parent process until he receives message from a child process
union senum semaphore2;
semaphore2.val=0;
semctl(semid, 1, SETVAL, semaphore2);

//for blocking any child process so parent process will certainly read child's unique message
union senum semaphore3;
semaphore3.val=1;
semctl(semid, 2, SETVAL, semaphore3);

//initialization
semopr[0].sem_num = 0;
semopr[0].sem_flg = 0;

semopr[1].sem_num = 1;
semopr[1].sem_flg = 0;

semopr[2].sem_num = 2;
semopr[2].sem_flg = 0;


// seting up shared memory segments

if ((shm_id1 = shmget ((key_t) 1234, 50*sizeof(int), 0666 | IPC_CREAT)) == -1 ) {
        printf ("Error in shared memory region setup 1.\n");
        exit (-2);
        } // if shared memory get failed

if ((shm_id2 = shmget ((key_t) 1233, 50*sizeof(int), 0666 | IPC_CREAT)) == -1 ) {
        printf ("Error in shared memory region setup 2.\n");
        exit (-2);
        } // if shared memory get failed
if ((shm_id3 = shmget ((key_t) 1334, 1*sizeof(int), 0666 | IPC_CREAT)) == -1 ) {
        printf ("Error in shared memory region setup 3.\n");
        exit (-2);
        } // if shared memory get failed




// attach the shared memory segments
addr = (int *) shmat (shm_id1, (int *) 0, 0);
priority = (int *) shmat (shm_id2, (int *) 0, 0);
in = (int *) shmat (shm_id3, (int *) 0, 0);


*in=-1;         //initialization



int max[50][50];
int alloc[50][50];
int avail[50];
int check[50];

int finish[50];
int processes=0,resources=atoi(argv[1]);
int steps_sec=atoi(argv[argc-4]),term_processes=0; //recomended value steps_sec>=40
int min_interval=atoi(argv[argc-3]);
int max_interval=atoi(argv[argc-2]);
int period=atoi(argv[argc-1]);

for(i=0;i<50;i++){
        finish[i]=0;
        for(j=0;j<50;j++){
                max[i][j]=-1;
                alloc[i][j]=-1;
        }
}

for(i=2;i<argc-5;i++){
        avail[i-2]=atoi(argv[i]);
        fprintf(stderr,"init avail = %d\n",avail[i-2]);
}
//resources/ avail/ processes/ steps/
/*
fprintf(stderr,"resources = %d\n",resources);
fprintf(stderr,"child_processes = %d\n",child_processes);
fprintf(stderr,"steps_sec = %d\n",steps_sec);
fprintf(stderr,"min_interval = %d\n",min_interval);
fprintf(stderr,"max_interval = %d\n",max_interval);
fprintf(stderr,"period = %d\n",period);
*/

//creating ALL child processes
for(i=0;i<child_processes;i++){
        pid[i]=fork();
        if(pid[i]==0) break; //child process
        if (pid[j]>0) continue; //parent process
        if (pid[i]==-1) {perror("fork"); cleanup(semid,shm_id1,shm_id2,shm_id3, addr,priority,in); exit(-3);}
}

if(i==child_processes){//parent process
        fprintf(stderr,"Parent Process Id : %d\n",getpid());
        int flag=0,cont=1,occ=0;
        time_t start_time = time(NULL);
        time_t now=0,diff=0;

        while(diff<steps_sec){ //as long as the time frame allows repeat following code

                if(term_processes==child_processes) break;
                now = time(NULL);
                diff = now - start_time;

                if(cont==1){
                        occ++;
                        fprintf(stderr,"occ %d and alive %d\n",occ,alive_processes);
                        if(occ==alive_processes) {cont=0; occ=0;}
                        semopr[1].sem_op = -1;   
                        sem=semop(semid, &semopr[1], 1);   
                        //perror("SEMAPHORE 1 DOWN");             
                        if(sem==-1) {strerror(errno); cleanup(semid,shm_id1,shm_id2,shm_id3, addr,priority,in); exit(-3);}
                                       
//--------------------------------------------------------------
                        semopr[0].sem_op = -1;                    
                        sem=semop(semid, &semopr[0], 1);
                        //perror("SEMAPHORE 0 DOWN");
                        if(sem==-1) {strerror(errno); cleanup(semid,shm_id1,shm_id2,shm_id3, addr,priority,in); exit(-3);}
//--------------------CS----------------------------------------
                        for(k=0;k<resources;k++) buffer[k]=addr[k];
//--------------------CS----------------------------------------
                        semopr[0].sem_op = 1;                    
                        sem=semop(semid, &semopr[0], 1);
                        //perror("SEMAPHORE 0 UP");
                        if(sem==-1) {strerror(errno); cleanup(semid,shm_id1,shm_id2,shm_id3, addr,priority,in); exit(-3);}

//--------------------------------------------------------------

                        semopr[2].sem_op = 1;                    
                        sem=semop(semid, &semopr[2], 1);
                        //perror("SEMAPHORE 2 UP");
                        if(sem==-1) {strerror(errno); cleanup(semid,shm_id1,shm_id2,shm_id3, addr,priority,in); exit(-3);}

                        for(k=0;k<resources;k++){
                                max[processes][k]=buffer[k];
                        }

                        if(max[processes][0]!=-1){
                                for(a=0;a<resources;a++){
                                        check[a]=avail[a]-max[processes][a];
                                        if(check[a]>=0) alloc[processes][a]=max[processes][a];
                                        else alloc[processes][a]=avail[a];
                                        avail[a]=avail[a]-alloc[processes][a];
                                }                                    
                        }
                
                        processes++;
                }
                //show(alloc,max,avail,processes,resources);
                
                if (((diff % period) == 0) && (diff!=0) && (flag==0))
                {
                        /* period seconds have passed */
                        cal(alloc,max,avail,processes,resources,finish);

                        flag=1; //to ensure we will call only once the function

                        for(k=0;k<processes;k++){
                                if(finish[k]==1){
                                        //fprintf(stderr,"finish[%d] = %d\n",k,finish[k]);
                                        for(a=0;a<resources;a++){
                                                avail[a]+=alloc[k][a];
                                                max[k][a]=0; 
                                                alloc[k][a]=0;
                                        }
                                        //fprintf(stderr,"terminating process[%d] %d\n",k,priority[k]);
                                        kill(priority[k],SIGTERM); //REQUEST TO CHILD PROCESS TO TERMINATE
                                        finish[k]=-1;
                                        term_processes++;
                                        alive_processes--;       

                                        cont=0;
                                        fprintf(stderr,"alive processes are %d\n",alive_processes);
                                }
                                else if(finish[k]==0){
                                        //fprintf(stderr,"finish[%d] = %d\n",k,finish[k]);
                                        fprintf(stderr,"re-running deadlocked process %d\n",priority[k]);
                                        kill(priority[k],SIGCONT);
                                        for(a=0;a<resources;a++){
                                                avail[a]+=alloc[k][a];
                                                max[k][a]=0; 
                                                alloc[k][a]=0;
                                        }
                                        finish[k]=-2;        
                                        cont=1;
                                }
                        } 
                }
                if((diff % period)!=0){ flag=0;}
      
                 
        //show(alloc,max,avail,processes,resources);
        }
        for(k=0;k<processes;k++){
                if(finish[k]==0 || finish[k]==1){
                        fprintf(stderr,"terminating process[%d] %d\n",k,priority[k]);  
                        kill(priority[k],SIGTERM); //REQUEST TO CHILD PROCESS TO TERMINATE
                }        
        }  
        signal(SIGQUIT, SIG_IGN);
        kill(0,SIGQUIT);                //finishing-of any survived child processes   
}
else if(pid[i]==0){//child process
        int flag=0,buffer[50],m=0;

        while(1){
                //child process retreats for random time to simulate random generation of child processes
                srand(getpid());
                interval=max_interval + rand() / (RAND_MAX / (min_interval - max_interval + 1) + 1);
                sleep(interval);

                semopr[2].sem_op = -1;                    
                sem=semop(semid, &semopr[2], 1);
                //perror("SEMAPHORE 2 IN CHILD DOWN");
                if(sem==-1) {strerror(errno); cleanup(semid,shm_id1,shm_id2,shm_id3, addr,priority,in); exit(-3);}

//--------------CS--------------------------------------------------------
                semopr[0].sem_op = -1;                    
                sem=semop(semid, &semopr[0], 1);
                //perror("SEMAPHORE 0 IN CHILD DOWN");
                if(sem==-1) {strerror(errno); cleanup(semid,shm_id1,shm_id2,shm_id3, addr,priority,in); exit(-3);}
        
                (*in)++;
                priority[*in]=getpid();
                if(flag==0){
                        for(k=0;k<resources;k++){
                                m=avail[k];
                                addr[k]=m+rand()/(RAND_MAX/(0-m+1)+1);
                                buffer[k]=addr[k];
                        }
                }
                else{
                        for(k=0;k<resources;k++){
                                addr[k]=buffer[k];
                        }                        
                }
                flag=1;
                semopr[0].sem_op = 1;                    
                sem=semop(semid, &semopr[0], 1);
                //perror("SEMAPHORE 0 IN CHILD UP");
                if(sem==-1) {strerror(errno); cleanup(semid,shm_id1,shm_id2,shm_id3, addr,priority,in); exit(-3);}
//------------------------------------------------------------------------
                semopr[1].sem_op = 1;                    
                sem=semop(semid, &semopr[1], 1); 
                //perror("SEMAPHORE 1 IN CHILD UP");
                if(sem==-1) {strerror(errno); cleanup(semid,shm_id1,shm_id2,shm_id3, addr,priority,in); exit(-3);}

                //fprintf(stderr,"stopping child %d\n",getpid());
                kill(getpid(),SIGTSTP);
        }  
        exit(0);                
}

cleanup(semid,shm_id1,shm_id2,shm_id3, addr,priority,in);

return 0;
}

