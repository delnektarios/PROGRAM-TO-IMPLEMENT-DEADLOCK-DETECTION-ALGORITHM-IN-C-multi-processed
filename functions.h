
//  NEKTARIOS DELIGIANNAKIS 1115201200030 sdi1200030@di.uoa.gr

#ifndef FUNCTIONS //include guard
#define FUNCTIONS

void cleanup (int semid,int shm_id1,int shm_id2,int shm_id3, int *addr,int *priority,int *in)

void cal(int (*alloc)[50],int (*max)[50],int (*avail),int processes,int resources,int (*finish))

void show(int (*alloc)[50],int (*max)[50],int (*avail),int processes,int resources)

int shm_id1,shm_id2,shm_id3; // ID's of shared memory segments

#endif
